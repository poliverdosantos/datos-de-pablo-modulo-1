const inicioDisplay = () => {
  inicio.style.display = "block";
  menu1.style.display = "none";
  menu2.style.display = "none";
  menu3.style.display = "none";
  menu4.style.display = "none";
  menu5.style.display = "none";
  menu6.style.display = "none";
};

const inicioLink = document.querySelector("#link1");
const menu1Link = document.querySelector("#link2");
const menu2Link = document.querySelector("#link3");
const menu3Link = document.querySelector("#link4");
const menu4Link = document.querySelector("#link5");
const menu5Link = document.querySelector("#link6");
const menu6Link = document.querySelector("#link7");

const inicio = document.querySelector("#inicio");
const menu1 = document.querySelector("#menu1");
const menu2 = document.querySelector("#menu2");
const menu3 = document.querySelector("#menu3");
const menu4 = document.querySelector("#menu4");
const menu5 = document.querySelector("#menu5");
const menu6 = document.querySelector("#menu6");

//Al abrir la pagina, ejecutamos el codigo en el evento onload

const cuerpo = document.body;
cuerpo.onload = inicioDisplay;

inicioLink.addEventListener("click", inicioDisplay);

menu1Link.addEventListener("click", () => {
  inicio.style.display = "none";
  menu1.style.display = "block";
  menu2.style.display = "none";
  menu3.style.display = "none";
  menu4.style.display = "none";
  menu5.style.display = "none";
  menu6.style.display = "none";
});

menu2Link.addEventListener("click", () => {
  inicio.style.display = "none";
  menu1.style.display = "none";
  menu2.style.display = "block";
  menu3.style.display = "none";
  menu4.style.display = "none";
  menu5.style.display = "none";
  menu6.style.display = "none";
});

menu3Link.addEventListener("click", () => {
  inicio.style.display = "none";
  menu1.style.display = "none";
  menu2.style.display = "none";
  menu3.style.display = "block";
  menu4.style.display = "none";
  menu5.style.display = "none";
  menu6.style.display = "none";
});

menu4Link.addEventListener("click", () => {
  inicio.style.display = "none";
  menu1.style.display = "none";
  menu2.style.display = "none";
  menu3.style.display = "none";
  menu4.style.display = "block";
  menu5.style.display = "none";
  menu6.style.display = "none";
});

menu5Link.addEventListener("click", () => {
  inicio.style.display = "none";
  menu1.style.display = "none";
  menu2.style.display = "none";
  menu3.style.display = "none";
  menu4.style.display = "none";
  menu5.style.display = "block";
  menu6.style.display = "none";
});

menu6Link.addEventListener("click", () => {
  inicio.style.display = "none";
  menu1.style.display = "none";
  menu2.style.display = "none";
  menu3.style.display = "none";
  menu4.style.display = "none";
  menu5.style.display = "none";
  menu6.style.display = "block";
});
