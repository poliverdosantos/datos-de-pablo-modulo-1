//querySelectorAll selecciona varios elementos del DOM(document object model)
const imagenes = document.querySelectorAll(".img-galeria");
const imagenesLight = document.querySelector(".agregar-imagen");
const contenedorLight = document.querySelector(".imagen-light");
const hamburger1 = document.querySelector(".hamburger");

console.log(imagenes);
console.log(imagenesLight);
console.log(contenedorLight);

imagenes.forEach(imagen => {
  imagen.addEventListener("click", () => {
    //Get atribute sirve para sacar las caracteristicas o atributos del array.
    console.log(imagen.getAttribute("src"));
    aparecerImagen(imagen.getAttribute("src"));
  })
})
//Aparecer la imagen
const aparecerImagen = (imagen) => {
  imagenesLight.src = imagen;
  /*Adicionar dinamicamente estilos*/
  contenedorLight.classList.toggle("show");
  imagenesLight.classList.toggle("showImage");
  /*a .hamburger se modifica el estilo con opacidad 0 se muestra el boton de hamburger*/
  hamburger1.style.opacity = "0";
};

//cerrar la imagen
contenedorLight.addEventListener("click", (e) => {
  console.log(e.target);
  if (e.target !== imagenesLight) {
    contenedorLight.classList.toggle("show");
    imagenesLight.classList.toggle("showImage");
    /* a .hamburguer se modifica el estilo con opacidad 1 y no se muestra el boton de hamburger */
    hamburger1.style.opacity = "1";
  }
})