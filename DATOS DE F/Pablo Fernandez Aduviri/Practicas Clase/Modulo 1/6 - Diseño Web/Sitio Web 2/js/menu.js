const hamburger = document.querySelector(".hamburger");

const menu = document.querySelector(".menu-navegacion");

console.log(hamburger);
console.log(menu);

//Adicionar una clase css dinamicamente al html

//Keypress Keydown
//Al hacer click ocurrira algun evento el addEventListener necesita 2 parametros  el evento y que va a hacer cuando se presione

// classList accede a la lista de clases de un elemento toggle cuando solo hay un argumento presente alterna el valor de la clase por ejemplo: si  la clase  existe la elimina y devuelve false, si no la añade y devuelve true
hamburger.addEventListener("click", () => {
  //console.log("hamburger");
  menu.classList.toggle("spread");

});

/*Se ejecuta cuando se da click a cualquier parte del navegador*/
window.addEventListener("click", (e)=> {
  //console.log(e.target);

  //Verificamos si se tiene la clase de .menu-navegacion
  if (menu.classList.contains("spread") && e.target !== menu && e.target !== hamburger) {
    menu.classList.toggle("spread");
  }
});