const formulario  = document.querySelector("#formulario-contacto");
const inputs = document.querySelectorAll("#formulario-contacto input");
const textarea = document.querySelector("#formulario-contacto textarea");

const expresions = {
  //    \ esto significa espacio
  nombre: /^[a-zA-ZñÑ\s]+$/,
  email: /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/,
  mensaje: /^[0-9a-zA-ZñÑ\s]+$/
};

const campos = {
  nombre: false,
  email: false,
  mensaje: false
};

const validarFormularioInput = (e) => {
  //Accedemos al valor que tiene el id
  console.log(e.target.id);

  switch(e.target.id){
    case "nombre":
      validarCampoInput(expresions.nombre, e.target, "nombre");
      break;
    case "email":
      validarCampoInput(expresions.name, e.target, "email");
      break;
  }
};

const validarCampoInput = (expresion, input, campo) => {
  //Hacemos una evaluacion de la expresion regular
  //Con value accedemos al input
  //test es para evaluar en javascript
  if(expresion.test(input.value)){
    //Remueve la clase
    /*grupo es un id en html campo es nombre y form control es una clase en html*/
    document.querySelector(`#grupo-${campo} .form-control`).classList.remove("form-control-error");
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.remove("formulario-input-error-activo");
    campos[campo] = true;
  } else {
    //Adiciona la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.add("form-control-error");
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.add("formulario-input-error-activo");
    campos[campo] = false;
  }
};

const validarFormularioTextArea = (e) => {

  if (expresions.mensaje.test(e.target.value)) {
    //Remuevo la clase
    document.querySelector("#grupo-mensaje #mensaje").classList.remove("form-control-error");
    document.querySelector("#grupo-mensaje .formulario-input-error").classList.remove("formulario-input-error-activo");
    campos["mensaje"] = true;
  } else {
    //Adiciona la clase
    document.querySelector(`#grupo-mensaje .form-control`).classList.add("form-control-error");
    document.querySelector(`#grupo-mensaje .formulario-input-error`).classList.add("formulario-input-error-activo");
    campos["mensaje"] = falso;
  }
};

inputs.forEach((input) => {
  //console.log(input);
  //apretas la tecla y cuando dejas de apretar dispara

  /*
  keyup cuando se presiona la tecla y se levanta el dedo, el evento se ejecuta 
  */
  input.addEventListener("keyup", 
  validarFormularioInput);

  /*blur es disparado cuando un elemento a perdido su foco*/
  input.addEventListener("blur", validarFormularioInput);
});

textarea.addEventListener("keyup", validarFormularioTextArea);

textarea.addEventListener("blur", validarFormularioTextArea);

formulario.addEventListener("submit", (e) => {
  e.preventDefault();

  console.log("Ingreso al boton");

  if (campos.nombre && campos.email && campos.mensaje) {
    let nombre = document.querySelector("#nombre");
    const email = document.querySelector("#email");
    let mensaje = document.querySelector("#mensaje");
    
    nombre = nombre.value.trim();
    mensaje = mensaje.value.trim();

    console.log(nombre);
    console.log(mensaje);

    console.log("Enviando...");

    document.querySelector(".formulario-mensaje-exito").classList.add("formulario-mensaje-exito-activo");
    //Limpiar todos los campos del formulario
    formulario.reset();
    //El escape te lo fabrica
    //El poceso de lo que quiero hacer viene aqui y en el timeout viene el tiempo en milisegundos
    setTimeout(() => {//Proceso
      document.querySelector(".formulario-mensaje-exito").remove("formulario-mensaje-exito-activo");
    }, 3000);//Tiempo en milisegundos
  }
});