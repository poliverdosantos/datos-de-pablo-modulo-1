const hamburger = document.querySelector(".hamb");
const nav = document.querySelector("header .container nav");
const hambIcono = document.querySelector(".hamb i");
const navLink = document.querySelectorAll("header .container nav a");

hamburger.addEventListener("click", (e) => {
  console.log(e.target);
  //Cancela el evento relacionado
  e.preventDefault();
  //Adicionamos la clase open
  nav.classList.toggle("open");
  //Adicionando icono de Font Awesome
  hambIcono.classList.toggle("fa-xmark");
});

navLink.forEach(enlace => {
  enlace.addEventListener("click", () => {
    nav.classList.toggle("open");
    //Adicionando icono de font awesome
    hambIcono.classList.toggle("fa-xmark");
  });
});