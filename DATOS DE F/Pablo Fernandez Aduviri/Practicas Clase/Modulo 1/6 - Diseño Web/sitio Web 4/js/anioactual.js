//Obtenemos el año actual
const anioActual = new Date();
//Obtenemos la propiedad del elemento con id anio
const anio = document.querySelector('#anio');
/*
Obtenemos la propiedad textContent para colocar un texto
obtenemos el año actual del objeto date y luego llamamos
al metodo getFullYear()
*/
anio.textContent = anioActual.getFullYear();