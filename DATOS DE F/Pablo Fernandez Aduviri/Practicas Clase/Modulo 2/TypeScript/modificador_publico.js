var Persona = /** @class */ (function () {
    function Persona(nombre_) {
        this.nombre = nombre_;
    }
    Persona.prototype.hablar = function (mensaje) {
        console.log(mensaje);
    };
    return Persona;
}());
var instancia = new Persona("Juan");
console.log(instancia.nombre);
instancia.hablar("hola");
