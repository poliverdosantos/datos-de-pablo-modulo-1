var Jugador = /** @class */ (function () {
    function Jugador(posicion_) {
        this.posicion = posicion_;
    }
    Jugador.prototype.obtener_posicion = function () {
        console.log(this.posicion);
        this.sobreescribir_posicion();
        console.log(this.posicion);
    };
    Jugador.prototype.sobreescribir_posicion = function () {
        this.posicion = "Arquero";
    };
    return Jugador;
}());
var Messi = new Jugador("Delantero");
//Modificador privado solo es accedido dentro de la clase
//console.log(Messi.posicion);
Messi.obtener_posicion();
