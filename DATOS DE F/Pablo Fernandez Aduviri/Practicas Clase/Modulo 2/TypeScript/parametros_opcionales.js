function nombre_completo(nombre, apellido) {
    return nombre + " " + apellido;
}
//En typescript los parametros son obligatorios
//En javascript los parametros no son obligatorios
var resultado = nombre_completo("Pablo", "Fernandez");
console.log(resultado, "resultado");
//Con el signo de interrogacion es un parametro opcional
//    El apellido es opcional porque tiene una interrogacion
function nombre_completo2(nombre, apellido) {
    console.log(apellido);
    if (apellido) {
        return nombre + " " + apellido;
    }
    else {
        return nombre;
    }
}
var resultado2 = nombre_completo2("Zacarias");
console.log(resultado2, "Resultado 2");
