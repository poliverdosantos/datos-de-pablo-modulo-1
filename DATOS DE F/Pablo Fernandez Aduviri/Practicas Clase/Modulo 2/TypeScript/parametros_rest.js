function Deportes(persona) {
    var deportes = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        deportes[_i - 1] = arguments[_i];
    }
    return "A ".concat(persona, " le gustan los siguientes deportes: ").concat(deportes.join(","));
}
console.log(Deportes("Barbara", "Futbol", "Beisbol", "Tennis"));
