function asignar_posicion(nombre, numero, posicion) {
    if (posicion === void 0) { posicion = "Defensa"; }
    return "".concat(nombre, " jugara con el numero ").concat(numero, " en la posicion de ").concat(posicion);
}
//En typescript todos los parametros son requeridos
var jugador1 = asignar_posicion("Pedro", 10, "Delantero");
console.log(jugador1);
var jugador2 = asignar_posicion("Juan", 23);
console.log(jugador2);
