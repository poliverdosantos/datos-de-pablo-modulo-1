class Vehiculo {
    //Atributos o propiedades sin inicializar

    marca: string;
    fecha_creacion: string;
    color: string;
    puertas: number;
    kilometraje: number;

    //constructor se inicializa las propiedades de un objeto

    //Constructor es opcional si se quiere se pone
    constructor (marca_: string, fecha_creacion_: string, color_: string, puertas_: number,  kilometraje_: number) {
        //con this se hace referencia a la propiedad que esta dentro de la clase
        this.marca = marca_;
        this.fecha_creacion = fecha_creacion_;
        this.fecha_creacion = fecha_creacion_;
        this.color = color_;
        this.puertas = puertas_;
        this.kilometraje =  kilometraje_;
    }

    //Metodos
    avanzar(): void{
        this.kilometraje = this.kilometraje + 100;
    }
}

let mi_vehiculo = new Vehiculo("Ford", "2019", "Negro", 2, 0);
//Acceder a un atributo
console.log(mi_vehiculo.kilometraje, "antes de avanzar");
mi_vehiculo.avanzar();
console.log(mi_vehiculo.kilometraje, "Despues de avanzar");
mi_vehiculo.avanzar();
console.log(mi_vehiculo.kilometraje, "Despues de avanzar 2 veces");
mi_vehiculo.avanzar();
console.log(mi_vehiculo.kilometraje, "Despues de avanzar 3 veces");

let mi_camioneta = new Vehiculo("Toyota", "2017", "Cafe", 4, 0);

console.log(mi_vehiculo.puertas);
console.log(mi_camioneta.puertas, "puertas de la camioneta");
console.log(mi_camioneta.kilometraje, "Kilometraje de la camioneta");







