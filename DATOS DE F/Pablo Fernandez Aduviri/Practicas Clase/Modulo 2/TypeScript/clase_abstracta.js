var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//Clase Padre
var SuperClase = /** @class */ (function () {
    function SuperClase() {
    }
    SuperClase.prototype.saludar = function () {
        console.log("Hola");
    };
    return SuperClase;
}());
//Clase Hija
var claseDerivada = /** @class */ (function (_super) {
    __extends(claseDerivada, _super);
    function claseDerivada() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    claseDerivada.prototype.metodo_a_sobreescribir = function () {
        console.log("codigo nuevo");
    };
    return claseDerivada;
}(SuperClase));
var instancia = new claseDerivada();
instancia.saludar();
instancia.metodo_a_sobreescribir();
//En una clase abstracta no puedo instanciar la clase padre me da error
//No puedo instanciar clasde padre
//let superclase = new SuperClase();
