var Estudiante = /** @class */ (function () {
    function Estudiante() {
    }
    Estudiante.prototype.mostrarNombreApellido = function () {
        return "".concat(this.nombre, " ").concat(this.apellido);
    };
    return Estudiante;
}());
var estudiante = new Estudiante();
estudiante.nombre = "David";
estudiante.apellido = "Rodriguez";
console.log(estudiante);
console.log(estudiante.mostrarNombreApellido());
