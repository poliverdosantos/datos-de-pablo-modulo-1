let permiso = true;
class Trabajador {
    get nombre_completo() {
        return this.nombre;
    }
    set colocar_nombre(nombre) {
        if (permiso) {
            this.nombre = nombre;
        }
        else {
            console.log("Sin permisos");
        }
    }
}
let empleado = new Trabajador();
empleado.nombre = "Pedro";
console.log(empleado);
console.log(empleado.nombre_completo);
empleado.colocar_nombre = "Juan";
console.log(empleado.nombre_completo);
