//Clase Padre
abstract class SuperClase {
    abstract metodo_a_sobreescribir():void;
    saludar():void {
    console.log("Hola");
    }
}
//Clase Hija
class claseDerivada extends SuperClase {
    metodo_a_sobreescribir():void {
    console.log("codigo nuevo");
    }
}
let instancia = new claseDerivada();
instancia.saludar();
instancia.metodo_a_sobreescribir();

//En una clase abstracta no puedo instanciar la clase padre me da error
//No puedo instanciar clasde padre
//let superclase = new SuperClase();