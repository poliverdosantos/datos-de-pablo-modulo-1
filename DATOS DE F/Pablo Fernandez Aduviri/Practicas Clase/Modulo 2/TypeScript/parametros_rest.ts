function Deportes(persona: string, ...deportes: string[]){
    return `A ${persona} le gustan los siguientes deportes: ${deportes.join(",")}`;
}

console.log(Deportes("Barbara", "Futbol", "Beisbol", "Tennis"));
