//Funciones en TS
//Devuelve un valor de tipo number
function Multiplicar(x: number, y: number): number{
    return x * y;
}

let producto: number = Multiplicar(2, 4);
console.log(producto, "producto");

let sumatoria1: number = 0;
let sumatoria2: number = 0;

console.log(sumatoria1, "Sumatoria 1 antes de ejecutar");
console.log(sumatoria2, "Sumatoria 2 antes de ejecutar");

//La funcion

function sumar(): void {
    ++sumatoria1;
}

function sumar2(): number {
    return ++sumatoria2;
}

//El resultado es de tipo undefined
let resultado1 = sumar();

let resultado2: number = sumar2();
console.log(resultado1, "resultado1");
console.log(resultado2, "resultado2");
console.log(sumatoria1, "Sumatoria 1 despues de ejecutar");
console.log(sumatoria2, "Sumatoria 2 despues de ejecutar");





