//Clase Padre
class Animales {
    nombre: string;
    constructor(nombre_:string) {
    //console.log("Entre en el constructor de animales");
        this.nombre = nombre_;
    }
    caminar(distancia: number = 0): void {
        console.log(`${this.nombre} camino ${distancia} metros`);
    }
}
//Clase Hija
class Serpiente extends Animales {
    longitud:number;
    constructor(nombre:string, longitud_: number){
    console.log("Entre en el constructor de serpiente");
    //Va al constructor de la clase Animales
    super(nombre);
    //Las asignaciones se colocan después del contructor de la clase padre
    this.longitud = longitud_;
    }
    caminar(distancia:number = 5) {
        console.log(`Deslizando ${distancia} metros`);
        //Va a la clase Animales y obtiene el metodo caminar
        //super.caminar(distancia);
    }
}
//Clase Hija
class Caballo extends Animales {    
    constructor(nombre:string) {
        //Va al constructor de la clase Animales
        super(nombre)
    }
}
let sam = new Serpiente("Sam la serpiente", 23);
let zeus = new Caballo("Zeus el caballo");
sam.caminar();
sam.caminar(33);
console.log(sam.longitud);
zeus.caminar();
zeus.caminar(55);
        
    

