interface Perro {
    //Atributo
    nombre: string;
}//Es como un objeto

function adoptar(mascota: Perro): void {
    console.log(`Yo adopte a ${mascota.nombre}`);
}

let mi_mascota: Perro = {nombre:"Tobby"};
adoptar(mi_mascota);