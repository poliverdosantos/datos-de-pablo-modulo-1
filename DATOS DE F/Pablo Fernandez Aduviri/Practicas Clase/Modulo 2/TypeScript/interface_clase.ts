interface Persona {
    nombre: string;
    apellido: string;
    mostrarNombreApellido(): string;
}
class Estudiante implements Persona {
    //Hay que implementar los atributos de la interfaz
    //La clase esta obligada a implementar esta interfaz
    nombre: string;
    apellido: string;
    mostrarNombreApellido(): string {
        return `${this.nombre} ${this.apellido}`;
    }
}
let estudiante = new Estudiante();
estudiante.nombre = "David";
estudiante.apellido = "Rodriguez";
console.log(estudiante);
console.log(estudiante.mostrarNombreApellido());

