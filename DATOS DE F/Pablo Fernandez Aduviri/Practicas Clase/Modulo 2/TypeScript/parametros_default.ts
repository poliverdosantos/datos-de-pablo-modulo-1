function asignar_posicion(nombre: string, numero: number, posicion: string = "Defensa"): string {
    return `${nombre} jugara con el numero ${numero} en la posicion de ${posicion}`;
}

//En typescript todos los parametros son requeridos
let jugador1: string = asignar_posicion("Pedro", 10, "Delantero");

console.log(jugador1);

let jugador2: string = asignar_posicion("Juan", 23);

console.log(jugador2);

