//Variables con tipo de dato
var soy_alto = true;
var soy_fan = false;
console.log(soy_alto, "soy_alto");
console.log(soy_fan, "soy_fan");
//operador de conjuncion
console.log(soy_alto && soy_fan, "conjuncion");
//operador de disyuncion
console.log(soy_alto || soy_fan, "disyuncion");
//metodo retorna el valor de la variable
console.log(soy_alto.valueOf());
