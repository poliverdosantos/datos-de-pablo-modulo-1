class Jugador {
    private posicion: string;

    constructor(posicion_: string) {
        this.posicion = posicion_;
    }

    obtener_posicion():void {
        console.log(this.posicion);
        this.sobreescribir_posicion();
        console.log(this.posicion);
    }

    private sobreescribir_posicion():void {
        this.posicion = "Arquero";
    }
}

let Messi = new Jugador("Delantero");

//Modificador privado solo es accedido dentro de la clase
//console.log(Messi.posicion);
Messi.obtener_posicion();