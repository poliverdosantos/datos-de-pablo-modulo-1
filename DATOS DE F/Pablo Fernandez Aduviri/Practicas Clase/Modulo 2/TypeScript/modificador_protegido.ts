class Instrumento {

    protected nombre: string;

    constructor(nombre_: string) {
        this.nombre = nombre_;
    }
}

//Clase Hija

class Piano extends Instrumento {

    private de_cuerda: boolean = false;
    constructor(nombre_: string) {
        //Con super se ingresa al constructor de la clase padre
        super(nombre_);    
    }

    public obtener_nombre(): void {
        console.log(this.nombre);
    }
}

let mi_piano = new Piano("Mi piano de cola");
mi_piano.obtener_nombre();