class Boligrafo {
    static marca: string;
    static color: string = "azul";

    static mostrarMarca(marca_: string){
        this.marca = marca_;
        console.log(this.marca);
    }
}
//console.log de console.log sale undefined
console.log(Boligrafo.mostrarMarca("Pilot"));
console.log(Boligrafo.color);

