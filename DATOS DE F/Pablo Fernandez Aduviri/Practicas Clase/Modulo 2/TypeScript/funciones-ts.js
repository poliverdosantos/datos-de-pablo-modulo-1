//Funciones en TS
//Devuelve un valor de tipo number
function Multiplicar(x, y) {
    return x * y;
}
var producto = Multiplicar(2, 4);
console.log(producto, "producto");
var sumatoria1 = 0;
var sumatoria2 = 0;
console.log(sumatoria1, "Sumatoria 1 antes de ejecutar");
console.log(sumatoria2, "Sumatoria 2 antes de ejecutar");
//La funcion
function sumar() {
    ++sumatoria1;
}
function sumar2() {
    return ++sumatoria2;
}
//El resultado es de tipo undefined
var resultado1 = sumar();
var resultado2 = sumar2();
console.log(resultado1, "resultado1");
console.log(resultado2, "resultado2");
console.log(sumatoria1, "Sumatoria 1 despues de ejecutar");
console.log(sumatoria2, "Sumatoria 2 despues de ejecutar");
