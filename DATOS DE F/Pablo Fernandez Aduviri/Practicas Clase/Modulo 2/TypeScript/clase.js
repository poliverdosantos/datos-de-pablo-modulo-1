var Vehiculo = /** @class */ (function () {
    //constructor se inicializa las propiedades de un objeto
    //Constructor es opcional si se quiere se pone
    function Vehiculo(marca_, fecha_creacion_, color_, puertas_, kilometraje_) {
        //con this se hace referencia a la propiedad que esta dentro de la clase
        this.marca = marca_;
        this.fecha_creacion = fecha_creacion_;
        this.fecha_creacion = fecha_creacion_;
        this.color = color_;
        this.puertas = puertas_;
        this.kilometraje = kilometraje_;
    }
    //Metodos
    Vehiculo.prototype.avanzar = function () {
        this.kilometraje = this.kilometraje + 100;
    };
    return Vehiculo;
}());
var mi_vehiculo = new Vehiculo("Ford", "2019", "Negro", 2, 0);
//Acceder a un atributo
console.log(mi_vehiculo.kilometraje, "antes de avanzar");
mi_vehiculo.avanzar();
console.log(mi_vehiculo.kilometraje, "Despues de avanzar");
mi_vehiculo.avanzar();
console.log(mi_vehiculo.kilometraje, "Despues de avanzar 2 veces");
mi_vehiculo.avanzar();
console.log(mi_vehiculo.kilometraje, "Despues de avanzar 3 veces");
var mi_camioneta = new Vehiculo("Toyota", "2017", "Cafe", 4, 0);
console.log(mi_vehiculo.puertas);
console.log(mi_camioneta.puertas, "puertas de la camioneta");
console.log(mi_camioneta.kilometraje, "Kilometraje de la camioneta");
