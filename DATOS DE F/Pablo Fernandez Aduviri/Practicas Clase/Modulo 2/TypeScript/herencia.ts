//Clase Padre
class Animal {
    caminar(distancia: number): void {
        console.log(`Se mueve ${distancia} metros`);
    }
}
//Clase Hija
class Gato extends Animal {
    maullar():void {
        console.log("Miauuuu");
    }
}
let gatito = new Gato();
gatito.maullar();
gatito.caminar(20);