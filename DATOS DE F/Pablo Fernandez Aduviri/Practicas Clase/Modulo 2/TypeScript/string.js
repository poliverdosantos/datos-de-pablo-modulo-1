var pelicula = "Mision Imposible";
var serie = 'Hannibal';
// template string
var poema = "\nHola\nComo\nEstas\n";
//operaciones
var programas = "Mis programas favoritos son: " + pelicula + " y " + serie;
console.log(programas);
//template string
var programas_2 = "Mis programas favoritos son: ".concat(pelicula, " y ").concat(serie, "\n");
console.log(programas_2);
var edad_actual = 23;
var edad_futura = "\nEl siguiente mes mi edad sera ".concat(edad_actual + 1);
console.log(edad_futura);
//metodos
//Obtiene el primer caracter de la cadena
console.log(programas.charAt(0));
console.log(programas.toUpperCase());
